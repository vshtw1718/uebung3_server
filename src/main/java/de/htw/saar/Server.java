package de.htw.saar;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;

public class Server {
    private static final Integer PORT = 8080;
    public static final String BASE_URI = "http://localhost:"+PORT.toString()+"/uebung3/";

    public static HttpServer startServer() {
        System.out.println(BASE_URI);
        final ResourceConfig rc = new ResourceConfig().packages("de.htw.saar");
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println("Server started : " + BASE_URI);
        System.in.read();
        server.shutdownNow();
    }
}
