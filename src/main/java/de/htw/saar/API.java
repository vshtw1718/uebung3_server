package de.htw.saar;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.math3.primes.Primes.isPrime;

@Path("/")
public class API {
    @GET
    @Path("/getPrimeNumbers")
    @Produces(MediaType.TEXT_PLAIN)
    public String getPrimzahlenAsString(@DefaultValue("2") @QueryParam("n") int n) {
        StringBuilder primenumbers = new StringBuilder();
        for(int i=2, anzahl=0; anzahl < n; i++) {
            if(isPrime(i)) {
                primenumbers.append(Integer.toString(i));
                anzahl++;
                //add delimiter
                if(anzahl < n) {
                    primenumbers.append(';');
                }
            }
        }

        return primenumbers.toString();
    }

    @GET
    @Path("/getPrimeNumbersAsInteger")
    @Produces(MediaType.APPLICATION_JSON)
    public Integer[] getPrimzahlenAsInteger(@DefaultValue("2") @QueryParam("n") int n) {
        Integer primenumbers[] = new Integer[n];
        for(int i=2, anzahl=0; anzahl < n; i++) {
            if(isPrime(i)) {
                primenumbers[anzahl] = i;
                anzahl++;
            }
        }

        return primenumbers;
    }

    @GET
    @Path("/getPrimeNumbers")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Primenumber> getPrimenumber(@DefaultValue("2") @QueryParam("n") int n) {
        List<Primenumber> primenumbers = new ArrayList<Primenumber>();
        Primenumber primenumber;

        for(int i=2, anzahl=0; anzahl < n; i++) {
            if(isPrime(i)) {
                primenumber = new Primenumber();
                primenumber.setName("Prime number "+Integer.toString(anzahl+1)+":");
                primenumber.setValue(i);
                primenumbers.add(primenumber);
                anzahl++;
            }
        }

        return primenumbers;


    }


}